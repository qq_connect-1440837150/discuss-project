﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DiscussProject.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreateTime { get; set; }
        public string Content { get; set; }
        public virtual int UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
        public virtual int PostId { get; set; }
        [ForeignKey("PostId")]
        public Article Post { get; set; }
    }
}