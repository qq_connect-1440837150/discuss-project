﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DiscussProject.Models
{
    public class Article
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreateTime { get; set; }
        public string Content { get; set; }
        public int Like { get; set; }
        public int ViewNum { get; set; }
        public string Category { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public virtual int UserId { get; set; }
        [ForeignKey("UserId")]
        public User Author { get; set; }
    }
    public class ArticleContext : DbContext
    {
        public ArticleContext() : base("name=DiscussProject")
        {

        }
        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<User> Users { get; set; }
    }
}