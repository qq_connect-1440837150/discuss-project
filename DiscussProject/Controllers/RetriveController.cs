﻿using DiscussProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DiscussProject.Controllers
{
    public class RetriveController : Controller
    {
        private ArticleContext db = new ArticleContext();
        private UserContext userdb = new UserContext();
        public JsonResult FindByCategory(string category)
        {
            var result = from item in db.Articles where item.Category.Equals(category) select item;
            var tmp = result.ToArray();
            for (int i = 0; i < tmp.Length; i++)
            {
                var author = db.Users.Find(tmp[i].UserId);
                tmp[i].Author = author;
            }
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FindByTitleLike(string title)
        {
            var result = from item in db.Articles where item.Title.Contains(title) select item;
            var tmp = result.ToArray();
            for (int i = 0; i < tmp.Length; i++)
            {
                var author = db.Users.Find(tmp[i].UserId);
                tmp[i].Author = author;
            }
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FindALL()
        {
            var datas = db.Articles;
            foreach (var item in datas)
            {
                item.Author = db.Users.Find(item.UserId);

            }
            return Json(datas, JsonRequestBehavior.AllowGet);
        }
    }
}