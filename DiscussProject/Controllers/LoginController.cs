﻿using DiscussProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DiscussProject.Controllers
{
    public class LoginController : Controller
    {
        [AllowAnonymous]
        [HttpPost]
        public ActionResult LoginMethod(string email, string pwd)
        {
            var user = UserMapper.QueryUserByEmail(email);
            if (user == null || user.Password != pwd)
            {
                TempData["login_ret"] = "用户名或密码错误!";
                return RedirectToAction("Login", "User");
            }
            Session["Id"] = user.Id;
            Session["Name"] = user.Name;
            if (user.Picture == null || user.Picture == "") user.Picture = "https://tva1.sinaimg.cn/crop.0.0.118.118.180/5db11ff4gw1e77d3nqrv8j203b03cweg.jpg";
            Session["head_img"] = user.Picture;
            return RedirectToAction("Index", "User");
        }
    }
}